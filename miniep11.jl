#Nome: Eduardo Ribeiro Silva de Oliveira
#NUSP: 11796920

function inverte!(texto)
        novo_texto = tratamento!(texto)
        return reverse(novo_texto)
end

using Unicode
function tratamento!(texto)
        #uma string vazia deve ser um palíndromo.
        #remover sinais, acentos e pontuações.
        novo_texto = []
        texto = uppercase(texto)
        if texto == ""
                return texto
        end
        for letter in texto
                if isletter(letter) #removendo sinais e pontuações
                        x = string(letter)
                        push!(novo_texto, Unicode.normalize(x, stripmark=true))
                        #removendo acentuações
                end
        end
        return novo_texto
end

function compara!(texto)
        if tratamento!(texto) == inverte!(texto)
                return true
        else
                return false
        end
end

using Test
function testes()
        println("Início dos testes")
        @test compara!("") == true
        @test compara!("ovo") == true
        @test compara!("MiniEP11") == false
        @test compara!("Socorram-me, subi no ônibus em Marrocos!") == true
        @test compara!("A mãe te ama.") == true
        @test compara!("Passei em MAC110!") == false
        @test compara!("Amo Omã. Se Roma me tem amores, amo Omã!") == true
        @test compara!("A Rita, sobre vovô, verbos atira.") == true
        @test compara!("Luza Rocelina, a namorada do Manuel, leu na moda da romana: anil é cor azul.") == true
        @test compara!("Ajudem Edu, já!") == true

        println("Fim dos testes!")
end

testes()
